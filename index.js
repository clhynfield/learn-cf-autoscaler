const express = require('express')
const app = express()
const port = process.env.PORT || 4000
const fast = 5
const slow = 200

app.responsiveness = fast

app.use(function (req, res, next) {
  console.log('Delaying ', app.responsiveness, 'ms')
  setTimeout(next, app.responsiveness)
})

app.get('/', function (req, res) {
  res.send('Hello World!')
  console.log('Said hello.')
})

app.get('/slow', function (req, res) {
  app.responsiveness = slow
  res.send('Hello, slow world!')
  console.log('Slowed down')
})

app.get('/fast', function (req, res) {
  app.responsiveness = fast
  res.send('Hello, fast world!')
  console.log('Sped up')
})

app.listen(port, function () {
  console.log('Example app listening on port ' + port + '!')
})
