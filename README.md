# Learn Cloud Foundry Autoscaler

## Create an Autoscaler service instance

```shell
cf create-service app-autoscaler standard autoscaler
```

## Push

```shell
cf push
```

This pushes and starts the app and binds it to the Autoscaler service instance.

## Test scheduled scaling

### Create a new scheduled limit change

```shell
instance_guid=$(cf service autoscaler | grep Dashboard) instance_guid="${instance_guid##*/}"
binding_guid=$(curl "https://autoscale.run.pivotal.io/api/instances/$instance_guid/bindings" -X GET -H "Authorization: $(cf oauth-token)" | jq -r '.resources[0].guid')
curl "https://autoscale.run.pivotal.io/api/bindings/$binding_guid/scheduled_limit_changes" \
    -X POST \
    -H "Authorization: $(cf oauth-token)" \
    -d "{
  \"executes_at\": \"$(TZ=UTC date -j -f %s $[$(date +%s)+10] +%Y-%m-%dT%H:%M:%SZ)\",
  \"min_instances\": 2,
  \"max_instances\": 3,
  \"enabled\": true
}"

```

## Test latency-based scaling

### Configure Autoscaler rules

FIXME - Manually for now, configure a single rule to scale according to HTTP latency:

- 1–3 instances
- down when < 10 ms
- up when > 100 ms

### Slow down app

```shell
curl $APP_BASE_URL/slow
```

### Make a few calls to trigger scaling

```shell
curl $APP_BASE_URL
curl $APP_BASE_URL
curl $APP_BASE_URL
```

### Observe Autoscaler state

FIXME - Manually for now, observe the state of the app in Autoscaler

### Speed up app

You may have scaled up to several instances, so to speed them all up:

```shell
curl $APP_BASE_URL/fast
curl $APP_BASE_URL/fast
curl $APP_BASE_URL/fast
```

### Make a few calls to trigger scaling

```shell
curl $APP_BASE_URL
curl $APP_BASE_URL
curl $APP_BASE_URL
```

### Observe Autoscaler state

FIXME - Manually for now, observe the state of the app in Autoscaler
